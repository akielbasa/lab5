﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Myjnia.Contract;
using Wyswietlacz.Contract;

namespace Myjnia.Impl
{
    public class Myjnia:IMycie
    {
        IWyswietlacz wyswietlacz;
        public Myjnia(IWyswietlacz wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }
        public void Myj()
        {
            wyswietlacz.DisplayForm("Myje samochod");
        }
        public void Susz()
        {
            wyswietlacz.DisplayForm("Suszy samochod");
        }
        public void Woskuj()
        {
            wyswietlacz.DisplayForm("Woskuje samochod");
        }
    }
}
