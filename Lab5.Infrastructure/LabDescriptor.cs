﻿using System;
using System.Reflection;
using Myjnia.Contract;
using Myjnia.Impl;
using Wyswietlacz.Impl;
using Wyswietlacz.Contract;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Kontener);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IMycie));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Myjnia.Impl.Myjnia));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz.Impl.Wyswietl));

        #endregion
    }
}
