﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PK.Container;
using System.Reflection;

namespace Lab5.Infrastructure
{
    class Kontener : IContainer
    {
        private readonly IDictionary<Type, Type> komponenty = new Dictionary<Type, Type>();
        private readonly IDictionary<Type, object> implementacje = new Dictionary<Type, object>();

        public void Register(Type type)
        {
            var interfaces = type.GetInterfaces();
            if (interfaces != null)
            {
                foreach (var item in interfaces)
                {
                    if (komponenty.ContainsKey(item) == false)
                    {
                        komponenty.Add(item, type);
                    }
                    else
                    {
                        komponenty[item] = type;
                    }
                }
            }
        }


        public void Register(Assembly assembly)
        {
            var classes = assembly.GetTypes();
            if (classes != null)
            {
                foreach (var type in classes)
                {

                    if (type.IsPublic)
                        Register(type);
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            var type = impl.GetType();
            var interfaces = type.GetInterfaces();
            foreach (var interf in interfaces)
            {
                if (!implementacje.ContainsKey(interf))
                {
                    implementacje.Add(interf, impl);
                }
                else
                {
                    implementacje[interf] = impl;
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            Register(provider.Invoke());
        }


        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            if (implementacje.ContainsKey(type))
            {
                return implementacje[type];
            }

            if (!komponenty.ContainsKey(type))
            {
                return null;
            }
            var imps = komponenty[type];

            var constructors = imps.GetConstructors();
            ConstructorInfo constrToSolve = constructors[0];
            int pcount = constrToSolve.GetParameters().Length;

            foreach (ConstructorInfo item in constructors)
            {
                var pinfo = item.GetParameters();
                if (pinfo.Length > pcount)
                {
                    pcount = pinfo.Length;
                    constrToSolve = item;
                }
            }

            var choosenParams = constrToSolve.GetParameters();

            if (choosenParams.Length == 0)
            {
                //return Activator.CreateInstance(imps);
            }


            List<object> parametery = new List<object>(choosenParams.Length);

            foreach (ParameterInfo parameterInfo in choosenParams)
            {
                var parameter = Resolve(parameterInfo.ParameterType);
                if (parameter == null)
                    throw new UnresolvedDependenciesException("Unresolved Dependecies");
                parametery.Add(parameter);
            }

            return constrToSolve.Invoke(parametery.ToArray());
        }
    }
}
