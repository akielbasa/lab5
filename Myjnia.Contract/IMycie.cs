﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Myjnia.Contract
{
    public interface IMycie:ISuszenie, IWoskowanie
    {
        void Myj();
    }
    public interface ISuszenie
    {
        void Susz();
    }
    public interface IWoskowanie
    {
        void Woskuj();
    }
}
