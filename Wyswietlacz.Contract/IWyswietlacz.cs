﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.DisplayForm;

namespace Wyswietlacz.Contract
{
    public interface IWyswietlacz
    {
        void DisplayForm(string tekst);
    }
}
