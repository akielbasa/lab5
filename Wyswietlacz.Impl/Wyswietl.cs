﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyswietlacz.Contract;
using Lab5.DisplayForm;
using System.Windows;

namespace Wyswietlacz.Impl
{
    public class Wyswietl:IWyswietlacz
    {
        public DisplayViewModel model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var form = new Form();
                var viewModel = new DisplayViewModel();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);
        public void DisplayForm(string tekst)
        {

            model.Text = tekst;

        }
    }
}
